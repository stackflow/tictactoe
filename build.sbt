name := "tictactoe"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % "1.3.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.akka" %% "akka-http" % "10.1.1",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.1.1" % "test",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.1",
  "com.typesafe.akka" %% "akka-stream" % "2.5.12",
  "com.typesafe.akka" %% "akka-slf4j" % "2.5.12",
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.12",
  "org.scalikejdbc" %% "scalikejdbc" % "3.2.3",
  "org.scalikejdbc" %% "scalikejdbc-config" % "3.2.3",
  "org.scalikejdbc" %% "scalikejdbc-test" % "3.2.3" % "test",
  "org.bouncycastle" % "bcprov-jdk15on" % "1.59",
  "org.scalikejdbc" %% "scalikejdbc-joda-time" % "3.2.2",
  "com.h2database" % "h2" % "1.4.197"
)

mainClass in(Compile, run) := Some("net.stackflow.tictactoe.WebServer")
