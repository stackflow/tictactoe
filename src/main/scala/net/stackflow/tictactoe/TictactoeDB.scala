package net.stackflow.tictactoe

import net.stackflow.tictactoe.user.{UserApi, UserModel}
import scalikejdbc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object TictactoeDB {

  def init(): Unit = {
    Class.forName("org.h2.Driver")
    ConnectionPool.singleton("jdbc:h2:mem:tictactoe;DB_CLOSE_DELAY=-1", "", "")
  }

  def createTables(): Future[Boolean] = {
    DB futureLocalTx { implicit session =>
      Future {
        sql"""
        create table users (
          id serial not null primary key auto_increment,
          username varchar(20) unique not null,
          pass varchar(64) not null,
          wins int not null,
          loses int not null,
          session varchar(64) not null,
          session_date timestamp null
        )
        """.execute.apply()
      }.map { _ =>
        sql"""
        create table games (
          id serial not null primary key auto_increment,
          next_step varchar(20) null,
          won varchar(20) null,
          finished boolean not null default false,
          initiator varchar(20),
          opponent varchar(20),
          steps int not null default 0,
          sizeX int not null,
          sizeY int not null,
          crosses_length_to_win int not null,
          cells varchar(200)
        )
        """.execute.apply()
      }
    }
  }

  def deleteTables(): Future[Boolean] = {
    DB futureLocalTx { implicit session =>
      Future {
        sql"""
        drop table IF EXISTS users
        """.execute.apply()
      }.map { _ =>
        sql"""
        drop table IF EXISTS games
        """.execute.apply()
      }
    }
  }

  def resetAll(): Future[Unit] = {
    TictactoeDB.deleteTables().map { _ =>
      TictactoeDB.createTables()
    }
  }

  def addSomeData(): Future[Any] = {
    DB futureLocalTx { implicit session =>
      UserModel.create(UserApi.requestToUserModel(UserApi.Request("vasya", "123456")))
    }
  }

}
