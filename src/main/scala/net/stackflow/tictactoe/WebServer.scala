package net.stackflow.tictactoe

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import net.stackflow.tictactoe.game.{DBGameService, GameServiceLike}
import net.stackflow.tictactoe.user.{DBUserRepository, UserRepositoryLike}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.io.StdIn

// Server definition
object WebServer extends Api {

  implicit val system: ActorSystem = ActorSystem("tictactoe-system")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext: ExecutionContext = system.dispatcher

  TictactoeDB.init()
  Await.result(TictactoeDB.createTables(), Duration.Inf)
  Await.result(TictactoeDB.addSomeData(), Duration.Inf)

  def main(args: Array[String]): Unit = {
    val bindingFuture: Future[Http.ServerBinding] = Http().bindAndHandle(route, "localhost", 8080)

    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }

  override val userRepo: UserRepositoryLike = DBUserRepository
  override val gameService: GameServiceLike = DBGameService

}
