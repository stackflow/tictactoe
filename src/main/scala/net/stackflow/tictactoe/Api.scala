package net.stackflow.tictactoe

import java.sql.Timestamp

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import net.stackflow.tictactoe.game.GameApi
import net.stackflow.tictactoe.user.UserApi
import spray.json._

trait TictactoeJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {

  implicit val timestampFormat = new JsonFormat[Timestamp] {

    def write(obj: Timestamp) = JsNumber(obj.getTime)

    def read(json: JsValue) = new Timestamp(json.convertTo[Long])

  }

  implicit val userRequestFormat = jsonFormat2(UserApi.Request.apply)

  implicit val userResponseFormat = jsonFormat4(UserApi.Response.apply)

  implicit val userSessionFormat = jsonFormat1(UserApi.UserSession.apply)

  implicit val NewGameRequestFormat = jsonFormat4(GameApi.NewGameRequest.apply)

  implicit val StepRequestFormat = jsonFormat1(GameApi.StepRequest.apply)

  implicit val GameResponseFormat = jsonFormat9(GameApi.GameResponse.apply)

}

trait WithCustom {

  def withAuth(headerName: String): Directive0 = optionalHeaderValueByName(headerName).flatMap {
    case Some(k) if k == "123" => pass
    case _ => complete(StatusCodes.Unauthorized)
  }

  def withAdminAuth(headerName: String): Directive0 = optionalHeaderValueByName(headerName).flatMap {
    case Some(_) => pass
    case _ => complete(StatusCodes.Unauthorized)
  }

}

trait Api extends UserApi with GameApi with WithCustom {

  implicit val sessionTime: Int = 10000  // session lifetime in millis, TODO to config

  val route: Route = {
    pathPrefix("debug") {
      path("reset") {
        post {
          withAdminAuth("admin") {
            onSuccess(TictactoeDB.resetAll()) {
              complete(StatusCodes.OK)
            }
          }
        }
      }
    } ~ userRoute ~ gameRoute
  }

}
