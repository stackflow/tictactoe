package net.stackflow.tictactoe.game

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import net.stackflow.tictactoe.TictactoeJsonSupport
import net.stackflow.tictactoe.user.UserRepositoryLike
import org.joda.time.DateTime

import scala.util.{Failure, Success}

object GameApi {

  case class NewGameRequest(opponent: String,
                            size: Seq[Int],
                            first_step_by: String,
                            crosses_length_to_win: Int)

  case class StepRequest(step: Seq[Int])

  case class GameResponse(id: Option[Long],
                          next_step: Option[String],
                          won: Option[String],
                          finished: Boolean,
                          players: Seq[String],
                          steps: Int = 0,
                          size: Seq[Int],
                          crosses_length_to_win: Int,
                          field: Array[Array[Int]])

  object GameResponse {

    def apply(game: GameModel): GameResponse = {
      GameResponse(
        id = game.id,
        next_step = game.nextStep,
        won = game.won,
        finished = game.finished,
        players = game.players,
        steps = game.steps,
        size = Seq(game.size._1, game.size._2),
        crosses_length_to_win = game.crossesLengthToWin,
        field = game.field
      )
    }
  }

}

trait GameApi extends TictactoeJsonSupport {

  implicit val sessionTime: Int

  val userRepo: UserRepositoryLike

  val gameService: GameServiceLike

  val gameRoute: Route = {
    pathPrefix("game") {
      pathEndOrSingleSlash {
        post {
          optionalHeaderValueByName("session") {
            case Some(sessionId) =>
              onSuccess(userRepo.findBySession(sessionId)) {
                case Some(initiator) if initiator.isOnline =>
                  userRepo.updateSession(initiator.username, initiator.session, Some(DateTime.now()))

                  entity(as[GameApi.NewGameRequest]) { r =>
                    onSuccess(userRepo.findByName(r.opponent)) {
                      case Some(opponent) =>
                        onComplete(GameModel.createAsync(initiator, opponent, r.first_step_by, r.size, r.crosses_length_to_win)) {
                          case Success(gameModel) =>
                            onSuccess(gameService.create(gameModel)) { game =>
                              complete(GameApi.GameResponse(game))
                            }

                          case Failure(ex) =>
                            complete(StatusCodes.BadRequest, ex.getMessage)
                        }

                      case _ =>
                        complete(StatusCodes.BadRequest, "Opponent not found")
                    }
                  }

                case _ =>
                  complete(StatusCodes.Unauthorized)
              }

            case _ =>
              complete(StatusCodes.Unauthorized)
          }
        } ~ get {
          parameters('limit.as[Int].?, 'offset.as[Int].?) { (limitOpt, offsetOpt) =>
            onSuccess(gameService.findAll(limitOpt, offsetOpt)) { games =>
              complete(games.map(GameApi.GameResponse(_)))
            }
          }

        }
      } ~ path(LongNumber) { gameId =>
        get {
          onSuccess(gameService.findById(gameId)) {
            case Some(game) =>
              complete(GameApi.GameResponse(game))

            case _ =>
              complete(StatusCodes.NotFound)
          }
        } ~ post {
          optionalHeaderValueByName("session") {
            case Some(sessionId) =>
              onSuccess(userRepo.findBySession(sessionId)) {
                case Some(player) if player.isOnline =>
                  userRepo.updateSession(player.username, player.session, Some(DateTime.now()))

                  entity(as[GameApi.StepRequest]) {
                    case r if r.step.length > 1 =>
                      onSuccess(gameService.findById(gameId)) {
                        case Some(game) =>
                          onComplete(GameModel.stepUpdated(game, player, r.step.head, r.step.tail.head)) {
                            case Success(updatedGame) =>
                              onSuccess(gameService.update(updatedGame)) { _ =>
                                if (updatedGame.finished && updatedGame.won.nonEmpty) {
                                  userRepo.increaseWins(player.username)
                                  userRepo.increaseLoses(updatedGame.players.filter(_ != player.username).head)
                                }
                                complete(GameApi.GameResponse(updatedGame))
                              }

                            case Failure(ex: UnsupportedOperationException) =>
                              complete(StatusCodes.Conflict, ex.getMessage)

                            case Failure(ex: IllegalArgumentException) =>
                              complete(StatusCodes.Forbidden, ex.getMessage)

                            case Failure(ex) =>
                              complete(StatusCodes.BadRequest, ex.getMessage)
                          }

                        case _ =>
                          complete(StatusCodes.NotFound)
                      }

                    case _ =>
                      complete(StatusCodes.BadRequest)
                  }

                case _ =>
                  complete(StatusCodes.Unauthorized)
              }

            case _ =>
              complete(StatusCodes.Unauthorized)
          }
        }
      }
    }
  }

}
