package net.stackflow.tictactoe.game

import scalikejdbc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait GameServiceLike {

  def create(game: GameModel): Future[GameModel]

  def update(game: GameModel): Future[GameModel]

  def findById(id: Long): Future[Option[GameModel]]

  def findAll(limit: Option[Int], offset: Option[Int]): Future[Seq[GameModel]]

}

object DBGameService extends GameServiceLike {

  private val t = GameModel.syntax

  def create(game: GameModel): Future[GameModel] = {
    DB futureLocalTx { implicit session =>
      val sql =
        sql"""INSERT INTO ${GameModel.table} (
          next_step, won, finished, initiator, opponent, steps, sizeX, sizeY, crosses_length_to_win, cells
        ) VALUES (
          ${game.nextStep},
          ${game.won},
          ${game.finished},
          ${game.players.head},
          ${game.players.tail.head},
          ${game.steps},
          ${game.size._1},
          ${game.size._2},
          ${game.crossesLengthToWin},
          ${game.field.flatten.mkString(",")}
        )"""
      Future {
        val id = sql.updateAndReturnGeneratedKey().apply()
        game.copy(id = Some(id))
      }
    }

  }

  def update(game: GameModel): Future[GameModel] = {
    DB futureLocalTx { implicit session =>
      val sql =
        sql"""UPDATE ${GameModel.table} SET
          next_step = ${game.nextStep},
          won = ${game.won},
          finished = ${game.finished},
          initiator = ${game.players.head},
          opponent = ${game.players.tail.head},
          steps = ${game.steps},
          sizeX = ${game.size._1},
          sizeY = ${game.size._2},
          crosses_length_to_win = ${game.crossesLengthToWin},
          cells = ${game.field.flatten.mkString(",")}
        WHERE
          id = ${game.id}
        """
      Future {
        sql.update.apply()
        game
      }
    }
  }

  def findById(id: Long): Future[Option[GameModel]] = {
    DB futureLocalTx { implicit session =>
      Future {
        val sql = withSQL(select.from[GameModel](GameModel as t).where.eq(t.id, id))
        sql.map(GameModel(_)).headOption().apply()
      }
    }
  }

  override def findAll(limitOpt: Option[Int], offsetOpt: Option[Int]): Future[Seq[GameModel]] = {
    (limitOpt.getOrElse(10), offsetOpt.getOrElse(0)) match {
      case (limit, _) if limit < 1 || limit > 10000 =>
        Future.failed(new IllegalArgumentException("Limit value is wrong"))

      case (_, offset) if offset < 0 =>
        Future.failed(new IllegalArgumentException("Offset value is wrong"))

      case (limit, offset) =>
        DB futureLocalTx { implicit session =>
          Future {
            val sql = withSQL {
              select.from[GameModel](GameModel as t)
                .orderBy(t.id)
                .limit(limit)
                .offset(offset)
            }
            sql.map(GameModel(_)).list().apply()
          }
        }
    }
  }

}
