package net.stackflow.tictactoe.game

import net.stackflow.tictactoe.user.UserModel
import scalikejdbc.{WrappedResultSet, _}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class GameModel(id: Option[Long],
                     nextStep: Option[String],
                     won: Option[String] = None,
                     finished: Boolean = false,
                     players: Seq[String],
                     steps: Int = 0,
                     size: (Int, Int),
                     crossesLengthToWin: Int,
                     field: Array[Array[Int]])

object GameModel extends SQLSyntaxSupport[GameModel] {

  case class Position(row: Int, col: Int)

  override val tableName = "games"

  def apply(rs: WrappedResultSet): GameModel = {
    val sizeX = rs.int("sizeX")
    val sizeY = rs.int("sizeY")

    new GameModel(
      id = rs.longOpt("id"),
      nextStep = rs.stringOpt("next_step"),
      won = rs.stringOpt("won"),
      finished = rs.boolean("finished"),
      players = Seq(rs.string("initiator"), rs.string("opponent")),
      steps = rs.int("steps"),
      size = (sizeX, sizeY),
      crossesLengthToWin = rs.int("crosses_length_to_win"),
      field = rs.string("cells").split(",").map(_.toInt).grouped(sizeX).toArray
    )
  }

  def createAsync(initiator: UserModel, opponent: UserModel, firstStepBy: String, size: Seq[Int], crossesLengthToWin: Int): Future[GameModel] = {
    Future {
      if (opponent.username == initiator.username)
        throw new IllegalArgumentException("Can't start battle with yourself")

      if (!Seq(opponent.username, initiator.username).contains(firstStepBy.toLowerCase))
        throw new IllegalArgumentException("First step player not found")

      if (size.length < 2)
        throw new IllegalArgumentException("Wrong size length")

      val sizeX = size.head
      val sizeY = size.tail.head

      if (Seq(sizeX, sizeY, crossesLengthToWin).min < 3 || math.max(sizeX, sizeY) >= 10)
        throw new IllegalArgumentException("Size is too small or ambiguous")

      if (crossesLengthToWin > math.min(sizeX, sizeY))
        throw new IllegalArgumentException("Crosses length to win ambiguous")

      GameModel(
        id = None,
        nextStep = Some(firstStepBy.toLowerCase),
        players = Seq(initiator.username, opponent.username),
        size = (sizeX, sizeY),
        crossesLengthToWin = crossesLengthToWin,
        field = Array.fill(sizeX)(Array.fill(sizeY)(0))
      )
    }
  }

  def stepUpdated(game: GameModel, player: UserModel, stepX: Int, stepY: Int): Future[GameModel] = {
    Future {
      if (game.finished)
        throw new Exception("Game already over")

      if (!game.players.contains(player.username))
        throw new IllegalArgumentException("Player isn't a member of this game")

      if (!game.nextStep.contains(player.username))
        throw new Exception(s"This is not player(${player.username}) step")

      if (game.field.apply(stepX).apply(stepY) != 0)
        throw new UnsupportedOperationException(s"Cell is already busy")

      val cellValue = if (game.players.head == player.username) 1 else 2
      val field = game.field
      field(stepX)(stepY) = cellValue

      val playerPositions = getPlayerPositions(cellValue)(field)
      val (finished, won, nextStep) = if (checkWin(playerPositions, game.crossesLengthToWin)) {
        (true, Some(player.username), None)
      } else if (getAvailablePositions(field).isEmpty) {
        (true, None, None)
      } else {
        (false, None, Some(game.players.filter(_ != player.username).head))
      }

      game.copy(
        nextStep = nextStep,
        won = won,
        finished = finished,
        steps = game.steps + 1,
        field = field
      )
    }
  }

  def getAvailablePositions(field: Array[Array[Int]]): Set[Position] = {
    getPlayerPositions(0)(field)
  }

  def getPlayerPositions(value: Int)(field: Array[Array[Int]]): Set[Position] = {
    getPositions(x => x == value)(field)
  }

  def getPositions(f: Int => Boolean)(field: Array[Array[Int]]): Set[Position] = {
    field.zipWithIndex.flatMap { case (array, x) =>
      array.zipWithIndex.flatMap { case (value, y) =>
        if (f(value)) Some(Position(x, y)) else None
      }
    }.toSet
  }

  def checkWin(positions: Set[Position], winLength: Int): Boolean =
    directions.exists(winConditionsSatisified(_)(positions, winLength))

  final val directions = List(leftDiagonal, rightDiagonal, row, pile)

  type StepOffsetGen = (Position, Int) => Position

  def leftDiagonal: StepOffsetGen = (pos, offset) => Position(pos.row + offset, pos.col + offset)

  def rightDiagonal: StepOffsetGen = (pos, offset) => Position(pos.row - offset, pos.col + offset)

  def row: StepOffsetGen = (pos, offset) => Position(pos.row + offset, pos.col)

  def pile: StepOffsetGen = (pos, offset) => Position(pos.row, pos.col + offset)

  def winConditionsSatisified(step: StepOffsetGen)
                             (positions: Set[Position], winLength: Int): Boolean =
    positions exists (position =>
      (0 until winLength) forall (offset =>
        positions contains step(position, offset)))

}