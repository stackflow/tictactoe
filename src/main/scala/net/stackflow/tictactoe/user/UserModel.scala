package net.stackflow.tictactoe.user

import java.time.{Instant, ZoneId, ZonedDateTime}
import java.util.TimeZone

import com.typesafe.scalalogging.LazyLogging
import org.bouncycastle.jcajce.provider.digest.SHA3
import org.bouncycastle.util.encoders.Hex
import org.joda.time.{DateTime, DateTimeZone}
import scalikejdbc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait UserModelLike extends LazyLogging {

  val sessionDate: Option[DateTime]

  def isOnline(implicit sessionTime: Int): Boolean = sessionDate.exists { date =>
    date.plusMillis(sessionTime).isAfterNow
  }

}

case class UserModel(id: Option[Long],
                     username: String,
                     pass: String,
                     wins: Int,
                     loses: Int,
                     session: String,
                     sessionDate: Option[DateTime]) extends UserModelLike

object UserModel extends SQLSyntaxSupport[UserModel] {

  override val tableName = "users"

  def apply(r: ResultName[UserModel])(rs: WrappedResultSet): UserModel = {
    new UserModel(
      rs.longOpt(r.id),
      rs.string(r.username),
      rs.string(r.pass),
      rs.int(r.wins),
      rs.int(r.loses),
      rs.string(r.session),
      rs.dateTimeOpt(r.sessionDate).map { zonedDateTime =>
        new DateTime(
          zonedDateTime.toInstant.toEpochMilli,
          DateTimeZone.forTimeZone(TimeZone.getTimeZone(zonedDateTime.getZone))
        )
      }
    )
  }

  def create(user: UserModel)(implicit session: DBSession = AutoSession): Future[UserModel] = {
    val c = UserModel.column
    val sql = withSQL(insert.into(UserModel).namedValues(
      c.id -> user.id,
      c.username -> user.username,
      c.pass -> user.pass,
      c.wins -> user.wins,
      c.loses -> user.loses,
      c.session -> user.session,
      c.sessionDate -> user.sessionDate.map { dt =>
        ZonedDateTime.ofInstant(Instant.ofEpochMilli(dt.getMillis), ZoneId.of(dt.getZone.getID, ZoneId.SHORT_IDS));
      }
    ))
    Future {
      val id = sql.updateAndReturnGeneratedKey().apply()
      user.copy(id = Some(id))
    }
  }

  def generateHash(string: String): String = {
    val md = new SHA3.DigestSHA3(256)
    md.update(string.getBytes("UTF-8"))
    Hex.toHexString(md.digest)
  }

  def toResponse(user: UserModel)(implicit sessionTime: Int): UserApi.Response = {
    UserApi.Response(
      username = user.username,
      online = user.isOnline,
      wins = user.wins,
      loses = user.loses
    )
  }

}

