package net.stackflow.tictactoe.user

import java.util.UUID

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import net.stackflow.tictactoe.TictactoeJsonSupport
import org.joda.time.DateTime

import scala.util.Success

object UserApi {

  case class Request(username: String,
                     password: String)

  case class Response(username: String,
                      wins: Int,
                      loses: Int,
                      online: Boolean)

  case class UserSession(session: String)

  def validateRequest(req: Request): Boolean = {
    if (req.username.length < 3 || req.username.length >= 20)
      false
    else if (req.password.length < 8 || req.password.length >= 100)
      false
    else
      true
  }

  def requestToUserModel(req: Request): UserModel = {
    UserModel(
      id = None,
      username = req.username.toLowerCase,
      pass = UserModel.generateHash(req.password),
      wins = 0,
      loses = 0,
      session = UUID.randomUUID.toString,
      sessionDate = None
    )
  }

}

trait UserApi extends TictactoeJsonSupport {

  implicit val sessionTime: Int

  val userRepo: UserRepositoryLike

  val userRoute: Route = {
    pathPrefix("user") {
      pathEndOrSingleSlash {
        post {
          entity(as[UserApi.Request]) { req =>
            if (UserApi.validateRequest(req)) {
              onComplete(userRepo.create(UserApi.requestToUserModel(req))) {
                case Success(_) => complete(StatusCodes.OK)
                case _ => complete(StatusCodes.Conflict)
              }
            } else {
              complete(StatusCodes.BadRequest)
            }
          }
        } ~ get {
          parameters('limit.as[Int].?, 'offset.as[Int].?) { (limitOpt, offsetOpt) =>
            onSuccess(userRepo.findAll(limitOpt, offsetOpt)) { users =>
              complete(users.map(UserModel.toResponse))
            }
          }
        }
      } ~ path("login") {
        post {
          entity(as[UserApi.Request]) { req =>
            onSuccess(userRepo.findByNameAndPassword(req.username, req.password)) {
              case Some(user) =>
                val session = UUID.randomUUID().toString
                onSuccess(userRepo.updateSession(user.username, session, Some(DateTime.now()))) {
                  case update if update == 1 =>
                    complete(UserApi.UserSession(session))

                  case _ =>
                    complete(StatusCodes.InternalServerError)
                }

              case _ =>
                complete(StatusCodes.Forbidden)

            }
          }
        }
      } ~ path("logout") {
        post {
          optionalHeaderValueByName("session") {
            case Some(sessionId) =>
              onSuccess(userRepo.findBySession(sessionId)) {
                case Some(user) if user.isOnline =>
                  onSuccess(userRepo.updateSession(user.username, sessionId, None)) {
                    case update if update == 1 =>
                      complete(StatusCodes.OK)

                    case _ =>
                      complete(StatusCodes.InternalServerError)
                  }

                case _ =>
                  complete(StatusCodes.Unauthorized)
              }

            case _ =>
              complete(StatusCodes.Unauthorized)

          }
        }
      } ~ path(Segment) { username =>
        get {
          onSuccess(userRepo.findByName(username)) {
            case Some(user) =>
              complete(UserModel.toResponse(user))
            case None =>
              complete(StatusCodes.NotFound, "user isn't found")
          }
        }
      }
    }
  }

}
