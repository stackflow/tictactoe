package net.stackflow.tictactoe.user

import java.time.{Instant, ZoneId, ZonedDateTime}

import org.joda.time.DateTime
import scalikejdbc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait UserRepositoryLike {

  def create(user: UserModel): Future[Unit]

  def findByName(username: String): Future[Option[UserModel]]

  def findByNameAndPassword(username: String, password: String): Future[Option[UserModel]]

  def findAll(limitOpt: Option[Int], offset: Option[Int]): Future[List[UserModel]]

  def updateSession(username: String, token: String, dateTime: Option[DateTime]): Future[Int]

  def findBySession(token: String): Future[Option[UserModel]]

  def increaseWins(username: String): Future[Boolean]

  def increaseLoses(username: String): Future[Boolean]

}

object DBUserRepository extends UserRepositoryLike {

  private val t = UserModel.syntax

  override def create(req: UserModel): Future[Unit] = {
    DB futureLocalTx { implicit session =>
      UserModel.create(req).map(_ => ())
    }
  }

  override def findByName(username: String): Future[Option[UserModel]] = {
    DB futureLocalTx { implicit session =>
      Future {
        val sql = withSQL(select.from[UserModel](UserModel as t).where.eq(t.username, username))
        sql.map(UserModel(t.resultName)).headOption().apply()
      }
    }
  }

  override def findByNameAndPassword(username: String, password: String): Future[Option[UserModel]] = {
    DB futureLocalTx { implicit session =>
      Future {
        val sql = withSQL {
          select.from[UserModel](UserModel as t)
            .where
            .eq(t.username, username)
            .and
            .eq(t.pass, UserModel.generateHash(password))
        }
        sql.map(UserModel(t.resultName)).headOption().apply()
      }
    }
  }

  override def findAll(limitOpt: Option[Int], offsetOpt: Option[Int]): Future[List[UserModel]] = {
    (limitOpt.getOrElse(10), offsetOpt.getOrElse(0)) match {
      case (limit, _) if limit < 1 || limit > 10000 =>
        Future.failed(new IllegalArgumentException("Limit value is wrong"))
      case (_, offset) if offset < 0 =>
        Future.failed(new IllegalArgumentException("Offset value is wrong"))
      case (limit, offset) =>
        DB futureLocalTx { implicit session =>
          Future {
            val sql = withSQL(select.from[UserModel](UserModel as t).orderBy(t.id).limit(limit).offset(offset))
            sql.map(UserModel(t.resultName)).list().apply()
          }
        }
    }
  }

  override def updateSession(username: String, token: String, dateTime: Option[DateTime]): Future[Int] = {
    DB futureLocalTx { implicit session =>
      Future {
        val c = UserModel.column
        withSQL {
          update(UserModel).set(
            c.session -> token,
            c.sessionDate -> dateTime.map { dt =>
              ZonedDateTime.ofInstant(Instant.ofEpochMilli(dt.getMillis), ZoneId.of(dt.getZone.getID, ZoneId.SHORT_IDS));
            }
          ).where.eq(c.username, username)
        }.update.apply()
      }
    }
  }

  override def increaseWins(username: String): Future[Boolean] = {
    DB futureLocalTx { implicit session =>
      Future {
        val sql =
          sql"""
                UPDATE ${UserModel.table} SET
                wins = wins + 1
                WHERE
                username = ${username}
            """
        sql.update.apply() == 1
      }
    }
  }

  override def increaseLoses(username: String): Future[Boolean] = {
    DB futureLocalTx { implicit session =>
      Future {
        val sql =
          sql"""
                UPDATE ${UserModel.table} SET
                loses = loses + 1
                WHERE
                username = ${username}
            """
        sql.update.apply() == 1
      }
    }
  }

  def findBySession(token: String): Future[Option[UserModel]] = {
    DB futureLocalTx { implicit session =>
      Future {
        val sql = withSQL {
          select.from[UserModel](UserModel as t)
            .where
            .eq(t.session, token)
        }
        sql.map(UserModel(t.resultName)).headOption().apply()
      }
    }
  }

}
